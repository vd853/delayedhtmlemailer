﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Microsoft.Win32;
using Utilities;

namespace EmailWordDelay
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow
	{
		// Use for all textbox
	    void OnFocusChangedSave(object s, RoutedEventArgs e)
	    {
	        OnDataChange();
	    }

		void OnRightClickClear(object s, RoutedEventArgs e)
		{
			var tb = (TextBox) s;
			tb.Text = "";
			Console.WriteLine("footer: " + B.HtmlExtra.Footer.ReferencePath);
		}
	    enum DialogType { Webpage, Text, JPEG, DE}
	    string LoadDialog<Control>(DialogType type, object sender)
	    {
	        var filter = new string[0];
	        var file = "null";
	        switch (type)
	        {
	            case DialogType.Webpage:
	                filter = new[] {"Webpage", ".html", "Webpage", ".htm"};
                    break;
	            case DialogType.Text:
	                filter = new[] { "Text file", ".txt" };
                    break;
	            case DialogType.JPEG:
	                filter = new[] {"JPEG Image", ".jpg"};
                    break;
	            case DialogType.DE:
	                filter = new[] {"Delay Email", ".DE"};
                    break;
	            default:
	                throw new ArgumentOutOfRangeException(nameof(type), type, null);
	        }
	        var diag = new OpenFileDialog();
	        diag.Filter = StringFormatter.DialogFilterGenerator(filter);
	        if (diag.ShowDialog() == true)
	        { 
	            if (typeof(Control) == typeof(TextBox))
	            {
	                var TB = (TextBox)sender;
                    TB.Text = diag.FileName;
                }
	            file = diag.FileName;
                return file;
	        }
	        return file;
        }
	    void OnFileSelectDE(object sender, RoutedEventArgs e)
	    {
	        var file = LoadDialog<MenuItem>(DialogType.DE, sender);
	        if (file == "null") return;
	        if (file != Directory.GetCurrentDirectory() + "\\" + defaultDataFile)
	        {
	            ValidateBuild();
	            BundleableModified = true;
                var tmp = _b.Clone();
	            var result = FileSerialization.ReFile<BundleData>(ref DataManager, ref tmp, file, false, true);
	            if (!result)
	            {
	                MessageBox.Show("Cannot load: " + Path.GetFileName(file));
	                return;
	            }
	            B = tmp.Clone();
                Logger("Loaded: " + Path.GetFileName(file));
            }
	        DataReload();
            ValidateBuild();
            PregenerateImage();
	        OnUseIndicate();
	        ToggleStarterControls(OperateMode.Ready);
	        Console.WriteLine(file);
	    }
        void OnFileSelectHTML(object sender, RoutedEventArgs e)
	    {
	        LoadDialog<TextBox>(DialogType.Webpage, sender);
        }
        void OnFileSelectText(object sender, RoutedEventArgs e)
		{
			var file = LoadDialog<TextBox>(DialogType.Text, sender);
            if(file != "null") OnDataChange();
        }
        void OnFileSelectJPEG(object sender, RoutedEventArgs e)
		{
		    LoadDialog<TextBox>(DialogType.JPEG, sender);
        }


		void OnPWLostFocus(object s, RoutedEventArgs e)
		{
			var pb = s as PasswordBox;
			B.EmailConfig.SenderPw = pb.Password;
			OnDataChange();
		}

		void OnURLEntry(object s, TextChangedEventArgs e)
		{
			var tb = s as TextBox;
			if (!StringValidator.IsURL(tb.Text))
			{
				tb.BorderBrush = Brushes.Crimson;
			}
			else
			{
				tb.BorderBrush = defaultBorderColor;
			}
		}

		void OnURLExit(object s, RoutedEventArgs e)
		{
			var tb = s as TextBox;
			if (!StringValidator.IsFullURL(tb.Text))
			{
				tb.BorderBrush = Brushes.Crimson;
			}
			else
			{
				tb.BorderBrush = defaultBorderColor;
			}
		}

		void OnLocalPathEntry(object s, TextChangedEventArgs e)
		{
			var tb = s as TextBox;
			tb.Text = tb.Text.Replace("\"", "");
			if (!File.Exists(tb.Text))
			{
				tb.BorderBrush = Brushes.Crimson;
			}
			else
			{
				tb.BorderBrush = defaultBorderColor;
			}
		}

		void OnFocusSelectall(object sender, RoutedEventArgs e)
		{
			var tb = sender as TextBox;
			tb.SelectAll();
		}
		private void EmailSender_LostFocus(object sender, RoutedEventArgs e)
		{
			var tb = sender as TextBox;
			if (String.IsNullOrEmpty(EmailServer.Text))
			{
				if (StringValidator.IsEmail(EmailSender.Text))
				{
					EmailServer.Text = StringFormatter.WebmailFromEmail(tb.Text);
				}
			}
			if (String.IsNullOrEmpty(TestEmail.Text)) TestEmail.Text = B.EmailConfig.Sender;
		}
		void OnDataChange()
		{
			ButtonToggle();
			ValidateBuild();
		    Console.WriteLine("data changed");
			//data will be saved on exit
		}
	
	}
}
