﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using EmailWordDelay.Annotations;

namespace EmailWordDelay
{
	//How this works? A unit of work is created from TimedUnit class. TimeUnit takes in a Func with some work that can fail or pass. TimeUnit also takes a the description of that work. Use handlerResult to attach a callback with triggerMethod. triggerMethod is on TimedMain, and will display the result on the UI. TimedMain is use to manage all the TimeUnit works. WorkData is a custom event arg in TimeUnit for returning data result data.
	public class TimedMain : INotifyPropertyChanged
	{
		public string LastProcess
		{
			get { return _lastProcess; }
			set { _lastProcess = value;}
		}

		public string NextProcessTime
		{
			get { return _nextProcessTime; }
			set { _nextProcessTime = value; }
		}

		public string CurrentTime
		{
			get { return _currentTime; }
			set { _currentTime = value;}
		}

		public int AmountRemaining
		{
			get { return _amountRemaining; }
			set { _amountRemaining = value;}
		}

		public string Countdown
		{
			get { return _countDown; }
			set { _countDown = value;}
		}

	    private Stopwatch sw;
		public int SecondsDelay;
		public CancellationTokenSource TokenSource;
		private List<TimedUnit> _processTask;
		private int _currentTask;
		private string _currentTime;
		private string _lastProcess;
		private string _nextProcessTime;
		private string _countDown;
		private DateTime _nextProcessTimeActual;
		private int _amountRemaining;
		private string _amountProcessed;
		public bool _isPaused;
		private Action _OnCompleted;
		private Action _OnCancelled;
		private Action<bool> _OnPaused;
		private List<Task> _processList;
		private BundleData _bundleData;

		public TimedMain(BundleData bundleData)
		{
			_bundleData = bundleData;
			//start timer to display current time
			Task.Run(() =>
			{
				while (true)
				{
					CurrentTime = DateTime.Now.ToString(Utilities.StringFormatter.DatetimeDDDHHDMMDSSPMAMFormat());
					Task.Delay(500).Wait();
				}
			});
			SetTimerInactive();
		}

		public void SetTimerInactive()
		{
			var msg = "Inactive";
			LastProcess = msg;
			NextProcessTime = msg;
			Countdown = msg;
			AmountRemaining = 0;
		}

		public void InitNewTask(List<TimedUnit> processTask,
			Action onCompleted,
			Action onCancelled,
			Action<bool> onPaused)
		{
			_processTask = processTask;
			_OnCompleted = onCompleted;
			_OnCancelled = onCancelled;
			_OnPaused = onPaused;
			_processList = new List<Task>();
			_currentTask = 0;
		    _isPaused = false;
			AmountRemaining = processTask.Count;
		    sw = new Stopwatch();
		    TokenSource = new CancellationTokenSource();
        }
		public void Pause()
		{
			_isPaused = !_isPaused;
			_OnPaused.Invoke(_isPaused);
		}

		public async Task Start(bool IsSequential)
		{
			await Task.Run(() =>
			{
                while (_currentTask < _processTask.Count)
				{
					if (TokenSource.IsCancellationRequested) return;
					if (!_isPaused) //NOT paused
					{
                        if(!sw.IsRunning) sw.Start();
						var nextEndAmount = _bundleData.Batch.AmountPerSend;
						if (nextEndAmount + _currentTask > _processTask.Count)
						{
							nextEndAmount = _processTask.Count - _currentTask;
						}
					    MainWindow.M.AsyncLogger("", false, true);
                        MainWindow.M.AsyncLogger(" ==== Process Section ==== ");
					    MainWindow.M.AsyncLogger("Next amount: " + nextEndAmount);
					    ProcessLoop(_currentTask, _currentTask + nextEndAmount, IsSequential);
					    sw.Restart();
                    }
					else //Paused
					{
                        if(sw.IsRunning) sw.Stop();
						_nextProcessTimeActual = DateTime.Now.AddSeconds(0);
                    }
					//waits for next process time
					while (sw.Elapsed < TimeSpan.FromSeconds(_bundleData.Batch.DelaySeconds) ||
                    !_processList.All(e=>e.IsCompleted))
					{
					    if (!_processList.All(e => e.IsCompleted))
					    {
					        Console.WriteLine("Task incomplete");
					    }
					    var remainder = TimeSpan.FromMinutes(_bundleData.Batch.DelayMinutes) - sw.Elapsed;
                        if (TokenSource.IsCancellationRequested) return;
						if (_processTask.All(a => a.hasCompleted)) return;
					    if (_isPaused)
					    {
					        if (sw.IsRunning) sw.Stop();
					    }
					    else
					    {
					        if (!sw.IsRunning) sw.Start(); 
                        }
					    _nextProcessTimeActual = DateTime.Now + remainder;
                        NextProcessTime = _nextProcessTimeActual.ToString(Utilities.StringFormatter.TimeSpanDDDHHDMMDSSFormat());
                        Task.Delay(500).Wait();
					    Countdown = remainder.ToString(Utilities.StringFormatter.TimeSpanDDDHHDMMDSSFormat());
                        Console.WriteLine("delayed");
					}
                }
			});

			Console.WriteLine("invokes completed");
			try
			{
				await Task.WhenAll(_processList.ToArray());
			}
			catch { }
			if (!TokenSource.IsCancellationRequested) _OnCompleted.Invoke();
            SetTimerInactive();
		}

		private void ProcessLoop(int start, int end, bool sequential)
		{
            //parallel task runs here
		    if (!sequential)
		    {
		        for (int i = start; i < end; i++)
		        {
		            var index = i;
		            var t = Task.Run(() =>
		            {
		                _processTask[index].handlerResult += triggerMethod;
		                _processTask[index].Invoke();
		                Console.WriteLine("Invoking: " + _processTask[index].WorkData.Description);
		            }, TokenSource.Token);
		            _processList.Add(t);
		            _currentTask++;
		        }
		        return;
		    }

            //sequential task runs here
		    var mainTask = Task.Run(() =>
		    {
		        for (int index = start; index < end; index++)
		        {
		            if (TokenSource.IsCancellationRequested)
		            {
		                break;
		            }
                    _processTask[index].handlerResult += triggerMethod;
		            _processTask[index].Invoke();
		            Console.WriteLine("Invoking: " + _processTask[index].WorkData.Description);
                    _currentTask++;
		        }
            }, TokenSource.Token);
            _processList.Add(mainTask);
		}
		public async void Stop()
		{
		    if (sw == null) return;
			if (TokenSource != null)
			{
				TokenSource.Cancel();
			}
			try
			{
				await Task.WhenAll(_processList.ToArray());
			}
			catch { }
			_OnCancelled.Invoke();
		}

		//Attach to handler by handlerResult+=triggerMethod;
		//This should be put somewhere externally
		public void triggerMethod(object s, WorkData e)
		{
			AmountRemaining--;
			LastProcess = e.Description;
			var result = "Sent";
			if (!e.Success) result = "Failed";
			MainWindow.M.AsyncLogger(e.Description + ": " + result);
			var sO = (TimedUnit)s;
			sO.handlerResult -= triggerMethod;
		}

		public event PropertyChangedEventHandler PropertyChanged;

		//[NotifyPropertyChangedInvocator]
		//protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		//{
		//	PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		//}
	}

	public class TimedUnit
	{
		public bool hasCompleted { get; set; }
		public Func<WorkData, WorkData> Work { get; set; }
		public WorkData WorkData { get; set; }
		public TimedUnit(Func<WorkData, WorkData> work, WorkData workData)
		{
			WorkData = workData;
			Work = work;
		}

		public void Invoke()
		{
			hasCompleted = true;
			Work.Invoke(WorkData);
			JustGotUpdated(WorkData);
		}
		public static TimedUnit TestUnitFactory(Func<WorkData, WorkData> work, WorkData reference)
		{
			return new TimedUnit(work, reference);
		}

		public event EventHandler<WorkData> handlerResult;

		protected virtual void JustGotUpdated(WorkData e)
		{
			if (handlerResult != null)
			{
				handlerResult(this, e);
			}
		}
	}
	public class WorkData : EventArgs //this should inhert some interface and be generated into this class
	{
		public string Description { get; set; }
		public bool Success { get; set; }
		public TimedMain TimedMain { get; set; }

		public WorkData(string description, TimedMain Controller)
		{
			TimedMain = Controller;
			Description = description;
		}
	}
}
