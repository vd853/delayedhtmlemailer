﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace EmailWordDelay
{
  public class Data
  {

    /// <summary>
    /// Copy properties of two objects of the same type
    /// </summary>
    /// <typeparam name="Type"></typeparam>
    /// <param name="Copy"></param>
    /// <param name="Paste"></param>
    public static void PropertyCopy<Type>(ref Type Copy, ref Type Paste)
    {
      foreach (var prop in Copy.GetType().GetProperties())
      {
        var Setter = Paste.GetType().GetProperty(prop.Name);
        //var value = GetValueFromProperty(Copy, prop.Name);
        var value = Copy.GetType().GetProperty(prop.Name).GetValue(Copy, null);
        Setter.SetValue(Paste, value, null);
      }
    }

    /// <summary>
    /// Compares two value of the same type and finds if any of their public property are different. It will return all the properties that are different
    /// </summary>
    /// <typeparam name="Type"></typeparam>
    /// <param name="Obj1"></param>
    /// <param name="Obj2"></param>
    /// <returns></returns>
    public static List<string> PropertyDifference<Type>(Type Obj1, Type Obj2)
    {
      var propertyDifference = new List<string>();
      foreach (var prop in Obj1.GetType().GetProperties())
      {
        var value1 = GetValueFromProperty(Obj1, prop.Name);
        var value2 = GetValueFromProperty(Obj2, prop.Name);
        if (value1 == null || value2 == null) continue;
        if (!value1.Equals(value2))
        {
          propertyDifference.Add(prop.Name);
        }
      }
      return propertyDifference;
    }

    /// <summary>
    /// Gets a value from its property
    /// </summary>
    /// <param name="value"></param>
    /// <param name="propName"></param>
    /// <returns></returns>
    public static object GetValueFromProperty(object value, string propName)
    {
      return value.GetType().GetProperty(propName).GetValue(value, null);
    }

    public static bool isEqualExtra<T>(T value1, T value2)
    {
      if (value1 == null)
      {
        if (value2 == null)
        {
          return true;
        }
      }
      if (value1.Equals(value2))
      {
        return true;
      }
      return false;
    }

    public static bool RandomBool()
    {
      Random random = new Random(Guid.NewGuid().GetHashCode());
      if (random.Next(0, 2) < 1)
      {
        return true;
      }
      return false;
    }

    public static int RandomInt(int min, int max)
    {
      Random random = new Random(Guid.NewGuid().GetHashCode());
      return random.Next(min, max);
    }
    public static bool IsOdd(int value)
    {
      return value % 2 != 0;
    }

    public static byte[] ObjectToByteArray(object obj)
    {
      BinaryFormatter bf = new BinaryFormatter();
      using (var ms = new MemoryStream())
      {
        bf.Serialize(ms, obj);
        return ms.ToArray();
      }
    }
    public static object ByteArrayToObject(byte[] arrBytes)
    {
      using (var memStream = new MemoryStream())
      {
        var binForm = new BinaryFormatter();
        memStream.Write(arrBytes, 0, arrBytes.Length);
        memStream.Seek(0, SeekOrigin.Begin);
        var obj = binForm.Deserialize(memStream);
        return obj;
      }
    }

    public static bool ByteArrayEquality(byte[] b1, byte[] b2)
    {
      return b1.SequenceEqual(b2);
    }

    /// <summary>
    /// Returns all variable and subvariable that contains an interface of T base on a given class
    /// </summary>
    /// <typeparam name="T">Interface to find</typeparam>
    /// <param name="Class">Class to dig into</param>
    /// <returns></returns>
    public static List<T> GetAllVariableOfInterfaceType<T>(object Class)
    {
      var Result = new List<T>();
      if(Class == null) return new List<T>();
      var prop = Class.GetType().GetProperties();
      if (!prop.Any()) return new List<T>();
      foreach (var type in prop)
      {
        try
        {
          var value = type.GetValue(Class);
          if (value == null) continue;
          Result = Result.Concat(GetAllVariableOfInterfaceType<T>(value)).ToList();
          if (value.GetType().GetInterfaces().Contains(typeof(T)))
          {
            Result.Add((T) value);
          }
        }
        catch
        {
          //cannot get type for value
        }
      }
      return Result;
    }

    /// <summary>
    /// Use this to for getting all the subvariable with type T inside a class object. It will return a string array of GetMethodNames. Cache this string array and later retrieve the subvariables with type T using GetAllVariableOfInterfaceTypeFromGetMethodName below.
    /// </summary>
    /// <typeparam name="T">Type to look for</typeparam>
    /// <param name="Class">object to search in</param>
    /// <returns></returns>
    public static List<string> GetAllVariableOfInterfaceTypeAsGetMethodName<T>(object Class, int Depth, int current = 0)
    {
      var Result = new List<string>();
      if (Class == null || current > Depth - 1) return new List<string>();
      var prop = Class.GetType().GetProperties();
      if (!prop.Any()) return new List<string>();
      foreach (var type in prop)
      {
        var value = type.GetValue(Class);
        if (value == null) continue;
        if (value.GetType().GetInterfaces().Contains(typeof(T)))
        {
          Result.Add(type.GetMethod.Name);
        }
        Result = Result.Concat(GetAllVariableOfInterfaceTypeAsGetMethodName<T>(value, Depth, current++)).ToList();
      }
      return Result;
    }
    /// <summary>
    /// See summery for GetAllVariableOfInterfaceTypeAsGetMethodName
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Class"></param>
    /// <param name="GetMethodNames">return value from GetAllVariableOfInterfaceTypeAsGetMethodName</param>
    /// <param name="Depth">Limit the search depth, two layers is 2</param>
    /// <param name="current">Currrent depth being searched</param>
    /// <returns></returns>
    public static List<T> GetAllVariableOfInterfaceTypeFromGetMethodName<T>(object Class, string[] GetMethodNames, int Depth, int current = 0)
    {
      var Result = new List<T>();
      if (Class == null || current > Depth - 1) return new List<T>();
      var prop = Class.GetType().GetProperties();
      if (!prop.Any()) return new List<T>();
      foreach (var type in prop)
      {
        try
        {
          var value = type.GetValue(Class);
          if (value == null || value == GetMethodNames) continue;
          if (GetMethodNames.Contains(type.GetMethod.Name))
          {
            Result.Add((T)value);
          }
          Result = Result.Concat(GetAllVariableOfInterfaceTypeFromGetMethodName<T>(value, GetMethodNames, Depth, current++)).ToList();
        }
        catch
        {
          //cannot get type for value
        }
      }
      return Result;
    }
  }
}
