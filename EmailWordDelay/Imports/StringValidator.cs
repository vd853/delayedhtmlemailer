﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace EmailWordDelay
{
    public static class StringValidator
    {
        public static bool isIPAddress(string value)
        {
            return Regex.IsMatch(value,
                "^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
        }

	    public static bool IsFullURL(string URL) //Must have http://....
	    {
		    return Uri.IsWellFormedUriString(URL, UriKind.Absolute);
	    }
        public static bool IsURL(string URL)
        {
            if (string.IsNullOrEmpty(URL)) { return false; }

            if (!URL.StartsWith("http://"))
            {
                URL = "http://" + URL;
            }

            Uri outWebsite;

            return Uri.TryCreate(URL, UriKind.Absolute, out outWebsite) && outWebsite.Host.Replace("www.", "").Split('.').Count() > 1 && outWebsite.HostNameType == UriHostNameType.Dns && outWebsite.Host.Length > outWebsite.Host.LastIndexOf(".") + 1 && 255 >= URL.Length;
        }
        public static bool OnlyPostiveNegativeNumbers(string input)
        {
            var re = new Regex(@"^\-?[0-9]\d{0,6}$"); //7 digits limit
            return re.IsMatch(input);
        }
        /// <summary>
        /// Verifies if the path is a valid windows path by looking at its format
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool PathVarify(string value)
        {
            Regex r = new Regex(@"^(([a-zA-Z]\:)|(\\))(\\{1}|((\\{1})[^\\]([^/:*?<>""|]*))+)$");
            if (r.IsMatch(value))
            {
                if (Directory.Exists(value))
                {
                    return true;
                }
            }
            return false;
        }
        public static bool OnlyLetterVarify(string value)
        {
            Regex rg = new Regex(@"^[a-zA-Z\w]+$");
            if (rg.IsMatch(value))
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// If search string is keyword. single word on seperated by commas
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool OnlyCommaKeywords(string value)
        {
            var rg = new Regex(@"^(\w+,)+\w+$");
            if (rg.IsMatch(value))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// If string is only numbers up to certain digit length
        /// </summary>
        /// <param name="value"></param>
        /// <param name="digitLimit"></param>
        /// <returns></returns>
        public static bool OnlyNumber(string value, int digitLimit, bool includeEmpty = false)
        {
            var rg = new Regex(@"^[0-9]{1," + digitLimit.ToString() + "}$");
            if (rg.IsMatch(value))
            {
                return true;
            }
            if (includeEmpty && value == "")
            {
                return true;
            }
            return false;
        }

        public static bool StringJustNumber(string test) //test if string only contains numbers
        {
            return Regex.IsMatch(test, @"^\d+$");
        }
        public static bool IsEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
      public static bool isEven(int test)
      {
        if (test % 2 == 0)
        {
          return true;
        }
        return false;
      }
  }
}
