﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using log4net.Appender;
using log4net.Core;
using log4net.Layout;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace EmailWordDelay
{
    //note, this is not static
    public class FileLogL4N
    {
        public log4net.ILog L = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //private log4net.LogManager manager;
        private PatternLayout file = new PatternLayout(@"%date{MMM-dd-yyyy HH:mm:ss},%level,%message%newline");
        private PatternLayout console = new PatternLayout(@"%message%newline");

        //Log file by this.L.debug()
        public FileLogL4N(string relativePath, int keep = 100, string size = "1MB")
        {
            //this will give you an error if you don't add the minimum settings in app.config
            log4net.Config.XmlConfigurator.Configure();

            //add appender
            ((log4net.Repository.Hierarchy.Logger)L.Logger).AddAppender(GetRollingFileAppender(relativePath, keep, size));
            ((log4net.Repository.Hierarchy.Logger)L.Logger).AddAppender(GetColoredConsoleAppender());

        }

        public static string GenerateFileName(bool isRelative = true, string folder = "", string fileAppend = "Log", bool isCSV = false, int upParent = 0)
        {
            var full = "";

            folder = folder + "\\";

            var relativeDirectory = "";
            if (isRelative)
            {
                relativeDirectory = Directory.GetCurrentDirectory() + "\\";
            } 

            full = relativeDirectory;

            if (upParent > 0)
            {
                full = StringFormatter.FolderUpParent(full, upParent);
            }

            full = full + folder + fileAppend;

            var suffix = "";
            if (isCSV)
            {
                suffix = ".csv";
            }
            else
            {
                suffix = ".txt";
            }

            return full + suffix;
        }

        private RollingFileAppender GetRollingFileAppender(string relativePath, int keep, string size)
        {
            var fileAppender = new RollingFileAppender();
            //settings for file roller
            fileAppender.Layout = file;
            fileAppender.MaximumFileSize = size;
            fileAppender.MaxSizeRollBackups = keep;
            fileAppender.File = relativePath;
            fileAppender.PreserveLogFileNameExtension = true;
            fileAppender.ActivateOptions();
            return fileAppender;
        }

        private ColoredConsoleAppender GetColoredConsoleAppender()

        {
            var consoleColorAppenderConsole = new ColoredConsoleAppender();
            //Color maps
            var InfoMap = new ColoredConsoleAppender.LevelColors();
            InfoMap.ForeColor = ColoredConsoleAppender.Colors.Yellow | ColoredConsoleAppender.Colors.HighIntensity;
            InfoMap.Level = Level.Info;
            var DebugMap = new ColoredConsoleAppender.LevelColors();
            DebugMap.ForeColor = ColoredConsoleAppender.Colors.Green | ColoredConsoleAppender.Colors.HighIntensity;
            DebugMap.Level = Level.Debug;
            var ErrorMap = new ColoredConsoleAppender.LevelColors();
            ErrorMap.ForeColor = ColoredConsoleAppender.Colors.Red | ColoredConsoleAppender.Colors.HighIntensity;
            ErrorMap.Level = Level.Error;
            //Settings for color console
            consoleColorAppenderConsole.Layout = console;
            consoleColorAppenderConsole.AddMapping(DebugMap);
            consoleColorAppenderConsole.AddMapping(ErrorMap);
            consoleColorAppenderConsole.AddMapping(InfoMap);
            consoleColorAppenderConsole.ActivateOptions();
            return consoleColorAppenderConsole;
        }
    }
    public class FileLog
    {
        private static string _fullLogPath; //the exact file path
        private string _logPath;
        public static int errorCount;
        public static int logFileCount; //is appended per session if over size limit to start a new file
        public static int sizeLimit = 1000000; //this is in bytes, when reach a new log is created, default is 1mb
        private static bool _createNew;
        private string _tag;
        /// <summary>
        /// Use to generate a log file name for every session
        /// </summary>
        /// <returns></returns>
        public static string GenerateFileName(bool isCSV = false)
        {
            if(isCSV) return DateTime.Now.ToString("MMddyyHHmmss") + "_log.csv";
            return DateTime.Now.ToString("MMddyyHHmmss") + "_log.txt";
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="text">Message</param>
        /// <param name="tag">Message Tag, example: ERROR</param>
        /// <param name="FileName">Leave blank for Log.txt</param>
        /// <param name="isError"></param>
        /// <param name="folderName">Leave blank for relative</param>
        /// <param name="createNew"></param>
        /// <param name="LogPath">Leave blank for relative</param>
        /// <param name="LimitCount">Amount to keep</param>
        public FileLog(string text, string tag, string FileName = "", bool isError = false, string folderName = "",
            bool createNew = false, string LogPath = "", int LimitCount = 0)
        {
            if (LogPath == "")
            {
                LogPath = Directory.GetCurrentDirectory();
            } 
            if (folderName != "")
            {
                LogPath = LogPath+ "\\" + folderName + "\\";
            }
            _logPath = LogPath;
            if (FileName == "")
            {
                FileName = "Log.txt";
            }
            _fullLogPath = LogPath + "\\" + FileName;

            _tag = tag + ",";
            outlog(text, isError);
            _createNew = createNew;
            if (LimitCount > 0)
            {
                ExceededCounts(LimitCount);
            }
        }

        public bool ExceededLimits(int mb)
        {
            //sizelimit checker, if limit reach, a new file will be created.. file name is based on time down to seconds
            if (!Directory.Exists(_fullLogPath)) return false;
            if (new FileInfo(_fullLogPath).Length > mb*sizeLimit) //this is in bytes
            {
                return true;
            }
            return false;
        }

        public void ExceededCounts(int length)
        {
            if (!Directory.Exists(_logPath)) return;
            var info = new DirectoryInfo(_logPath);
            FileInfo[] files = info.GetFiles().OrderBy(p => p.CreationTime).Reverse().ToArray();
            if (files.Length < length) return;
            var totalDelete = files.Length - length;
            for (int i = 0; i < totalDelete; i++)
            {
                files[files.Length-i-1].Delete();
            }
        }

        public static List<string> getLogFileListForm()
        {
            List<string> log = new List<string>();
            log.Add("** Current log information, check back for new entries. **");
            log.Add("Log path: " + _fullLogPath);
            log.Add("");
            using (var sr = new StreamReader(_fullLogPath))
            {
                while (sr.Peek() >= 0)
                {
                    log.Add(sr.ReadLine());
                }
            }
            return log;
        }

        private void outlog(string text, bool isError) //isError will mark as error count, otherwise just normal log
        {
            //create log folder if not exist
            if (!StringValidator.PathVarify(_logPath) || _createNew)
            {
                _createNew = false;
                FileInfo file = new System.IO.FileInfo(_logPath);
                file.Directory.Create();
            }

            try
            {
                using (StreamWriter filew = new StreamWriter(_fullLogPath, true))
                {
                    if (isError)
                    {
                        _tag = "ERROR," + _tag;
                        errorCount++;
                    }
                    else
                    {
                        _tag = "non-error," + _tag;
                    }
                    var completeMessage = DateTime.Now.ToString("MM/dd/yy HH:mm:ss") + "," + _tag + text;
                    filew.WriteLine(completeMessage);
                    filew.Close();
                }
            }
            catch
            {
                //("Cannot log file");
            } 
        }
  }

    //Log4Net minimum in app.config
    //<configSections>
    //<section name = "log4net"
    //type="log4net.Config.Log4NetConfigurationSectionHandler, log4net"/>
    //</configSections>
    //<log4net></log4net>

}
