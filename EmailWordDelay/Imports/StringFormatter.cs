﻿using System;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Text.RegularExpressions;
// using HtmlDocument = HtmlAgilityPack.HtmlDocument;

namespace EmailWordDelay
{
    public static class StringFormatter
    {
        public static string FolderUpParent(string original, int reverse)
        {
            for (int i = 0; i < reverse; i++)
            {
                original = Path.GetFullPath(Path.Combine(original, @"..\"));
            }
            return original;
        }
        //public static string HTMLBodyExtract(string RawHTML)
        //{
        //    if (RawHTML == null) return "";
        //    if (RawHTML == "") return "";
        //    var htmlDoc = new HtmlDocument();
        //    htmlDoc.LoadHtml(RawHTML);

        //    var htmlBody = htmlDoc.DocumentNode.SelectSingleNode("//body").InnerText;

        //    return htmlBody.Replace("&nbsp;", "").Replace(Environment.NewLine, "").Replace(" ", "").Trim();
        //}
        /// <summary>
        /// Trims a string to certain length and removes empty spaces or line breaks
        /// </summary>
        /// <param name="text"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public static string WordsOnly(string text, int limit = -1)
        {
            var reword = new string(text.Where(char.IsLetter).ToArray()); //keep only letters
            reword = Regex.Replace(reword, @" ", ""); //removes spaces
            reword = Regex.Replace(reword, Environment.NewLine, ""); //remove line breaks
            if (limit > 0)
            {
                if (reword.Length > limit)
                {
                    reword = reword.Substring(0, limit);
                }
            }
            return reword;
        }

        /// <summary>
        /// takes an int an words it into a string like 101 will be onehundredandone
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static string NumberToWords(int number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 1000000) > 0)
            {
                words += NumberToWords(number / 1000000) + " million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
                var tensMap = new[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return words;
        }

        /// <summary>
        /// Looks through a string and removes any duplicate words. Returns a string
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string dupeString(string text) //deduplicate words in string
        {
            var splitted = text.Split(' ');
            var deduplicateEnum = splitted.Distinct();
            string[] deduplicateArray = deduplicateEnum.Cast<string>().ToArray();
            var deduplicateString = String.Join(" ", deduplicateArray);
            return deduplicateString;
        }

        /// <summary>
        /// Removes weird char and extra spaces
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string CleanString(string text)
        {
            StringBuilder temp0 = new StringBuilder(text);
            var temp = temp0.ToString();
            temp = Regex.Replace(temp, @"[^\u0000-\u007F]", "?"); //remove werid char
            temp = Regex.Replace(temp, @"\s+", " "); //remove extra spaces
            var trimmed = temp.Trim();
            return trimmed;
        }

        public static int stringToInt(string value)
        {
            if (value == "") return 0;
            return Convert.ToInt32(value);
        }

        public static string GetDomainFromEmail(string email, bool withoutat = false)
        {
            int startIndex = email.IndexOf("@");
            if (withoutat)
            {
                startIndex++;
            }
            //int endIndex = email.IndexOf(".", startIndex);
            int domainLength = email.Length - startIndex;
            var domain = email.Substring(startIndex, domainLength);

            return domain;
        }

        public static SecureString StringToSecureString(string password)
        {
            if (password == null)
                throw new ArgumentNullException("password");

            var securePassword = new SecureString();

            foreach (char c in password)
                securePassword.AppendChar(c);

            securePassword.MakeReadOnly();
            return securePassword;
        }
        public static string RandomString(int length)
        {
            Random random = new Random(Guid.NewGuid().GetHashCode());
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
      public static string WebmailFromEmail(string email)
      {
        var atDomain = GetDomainFromEmail(email);
        return "webmail." + atDomain.Substring(1, atDomain.Length - 1);
      }
      public static string DialogFilterGenerator(string[] LabelAndExtension)
      {
        //var diag = new OpenFileDialog();
        //var fileName = "";
        //diag.Filter = "Delay Email (*.DE)|*.DE|All files (*.*)|*.*";
        //if (diag.ShowDialog() == true)
        //  fileName = diag.FileName;
        var sb = new StringBuilder();
        if (!StringValidator.isEven(LabelAndExtension.Length)) throw new Exception("Array must be even");
        for (int i = 0; i < LabelAndExtension.GetLength(0); i += 2)
        {
          sb.Append(String.Format("{0} (*{1})|*{1}|", LabelAndExtension[i], LabelAndExtension[i + 1]));
        }
        sb.Append("All files (*.*)|*.*");
        return sb.ToString();
      }
  }   
}
