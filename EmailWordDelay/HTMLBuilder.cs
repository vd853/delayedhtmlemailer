﻿using System.IO;
using System.Net.Mail;
using System.Net.Mime;

namespace EmailWordDelay
{
	public class HTMLBuilder
	{
		private readonly AlternateView _AltViewPrebuit;
		public HTMLBuilder(string ImagePath, BundleData bd)
		{
			_AltViewPrebuit = AltViewBuild(ImagePath, bd);
		}
		public MailMessage AppendPrebuilt(MailMessage MailtoAppend)
		{
			MailtoAppend.IsBodyHtml = true;
			MailtoAppend.AlternateViews.Add(_AltViewPrebuit);
			return MailtoAppend;
		}

		public static AlternateView AltViewBuild(string ImagePath, BundleData bd)
		{
			var FullHTML = HTMLBuild(bd.ImgData.Subject, ImagePath, true, bd.ImgData.Link, bd.ImgData.Width, bd.ImgData.Align, bd.HtmlExtra.Header.DatatoString(), bd.HtmlExtra.Footer.DatatoString());
			AlternateView Html = AlternateView.CreateAlternateViewFromString
				(FullHTML, null, MediaTypeNames.Text.Html);
			LinkedResource pic1 = new LinkedResource(ImagePath, MediaTypeNames.Image.Jpeg);
			pic1.ContentId = "Pic1";
			Html.LinkedResources.Add(pic1);
			return Html;
		}

		public static string HTMLBuild(string title,
		  string imageSource,
		  bool msgEmbbeded,
		  string link,
		  int widthPercentage,
		  string align,
		  string header = "",
		  string footer = "")
		{
			var titleHTML = "<title>" + title + "</title>"; 
			var imageSrc = "cid:Pic1"; //Pic1 is defined in LinkedResource
			if (!msgEmbbeded) imageSrc = imageSource;
			var veryTop = "<html>\r\n<body style=\'margin: 15\'>";
			var veryBottom = "</body>\r\n</html>";
			var headerCloser = "<div>" + header + "</div>";
			var footerCloser = "<div>" + footer + "</div>";
			var centerImage = "<div style=\"text-align:" + align.ToLower() + "\">\r\n\t<a href=\'" + link + "\'>\r\n\t\t<img src=\'" + imageSrc + "\' style=\'width: " + widthPercentage + "%\'>\r\n\t</a></div>";
			return titleHTML + veryTop + headerCloser + centerImage + footerCloser;
		}

		//Returns path so it can be deleted later
		public static string[] PreviewHTML(BundleData B, bool OnlyGenerate = false)
		{
			var previewFile = Directory.GetCurrentDirectory() + "/tmpPage.html";
			var imagePreview = Directory.GetCurrentDirectory() + "/tmpImg.jpg";
			ByteArrayToHiddenFile((byte[])B.ImgData.Image.Data, imagePreview);
			var FullHTML = HTMLBuild(B.ImgData.Subject, imagePreview, false,
			  B.ImgData.Link, B.ImgData.Width, B.ImgData.Align, B.HtmlExtra.Header.DatatoString(),
			  B.HtmlExtra.Footer.DatatoString());
			StringToHiddenFile(FullHTML, previewFile);
			if(!OnlyGenerate)System.Diagnostics.Process.Start(previewFile);
			return new[] { previewFile, imagePreview };
		}
		public static void ByteArrayToHiddenFile(byte[] data, string path)
		{
			try
			{
				File.WriteAllBytes(path, data);
			}
			catch
			{
			    try
			    {
			        File.Delete(path);
			        File.WriteAllBytes(path, data);
                }
                catch { }
			}
			File.SetAttributes(path, FileAttributes.Hidden);
		}
		public static void StringToHiddenFile(string data, string path)
		{
			try
			{
				File.WriteAllText(path, data);
			}
			catch
			{
			    try
			    {
			        File.Delete(path);
			        File.WriteAllText(path, data);
			    }
			    catch { }
            }
			File.SetAttributes(path, FileAttributes.Hidden);
		}
	}
}
