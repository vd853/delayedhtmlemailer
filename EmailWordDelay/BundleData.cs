﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using EmailWordDelay.Annotations;
using Utilities;

namespace EmailWordDelay
{
    public interface IValid
    {
        bool Valid { get; set; }
        void ValidateNow();
    }
    public class test : Data.ITClonable<test>
    {
        public string value { get; set; }
        public test Clone()
        {
            return new test(){value = value};
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }

    [Serializable]
    public class BundleData: Data.ITClonable<BundleData>
    {
        private EmailConfig _emailConfig;

        public EmailConfig EmailConfig
        {
            get { return _emailConfig; }
            set
            {
                _emailConfig = value;
            }
        }

        public HtmlExtra HtmlExtra { get; set; }
        public ImgData ImgData { get; set; }
        public Batch Batch { get; set; }
        public bool isAutoScroll { get; set; }
        public bool isFileLog { get; set; }
        public string[] IValidCache { get; set; }
        public static BundleData Factory()
        {
            var B = new BundleData();
            B.EmailConfig = new EmailConfig();
            B.HtmlExtra = new HtmlExtra();
            B.ImgData = new ImgData();
            B.Batch = new Batch();
            B.IValidCache = Data.GetAllVariableOfInterfaceTypeAsGetMethodName<IValid>(B, 2).ToArray();
            B.isAutoScroll = true;
            B.isFileLog = true;
            return B;
        }
        public List<IValid> GetSelfIValids()
        {
            return Data.GetAllVariableOfInterfaceTypeFromGetMethodName<IValid>((object)this, IValidCache, 2);
        }
        public static Tuple<string, bool, BundleData> BuildBundle(BundleData B)
        {
            var nullReturn = Tuple.Create("Null", false, new BundleData());
            var getEmailList = B.Batch.EmailList.DataBuilder<string[]>(B.Batch.EmailList.ReferencePath);
            if (!getEmailList.Item2)
            {
                return nullReturn;
            }
            try
            {
                var emptyCut = Int32.Parse(getEmailList.Item1.Split(':')[0]);
                var trims = Int32.Parse(getEmailList.Item1.Split(':')[1]);
                MainWindow.M.B.Batch.EmptyCut = emptyCut;
                MainWindow.M.B.Batch.Trims = trims;
            }
            catch
            {
                Console.WriteLine("no reload needed");
            }     
            var getImage = B.ImgData.Image.DataBuilder<byte[]>(B.ImgData.Image.ReferencePath);
            if (!getImage.Item2) return nullReturn;
            var getHeader = B.HtmlExtra.Header.DataBuilder<string>(B.HtmlExtra.Header.ReferencePath);
            if (!getHeader.Item2) return nullReturn;
            var getFooter = B.HtmlExtra.Footer.DataBuilder<string>(B.HtmlExtra.Footer.ReferencePath);
            if (!getFooter.Item2) return nullReturn;
            return Tuple.Create("Build Succesful", true, B);
        }
        public BundleData Clone()
        {
            return new BundleData()
            {
                EmailConfig = EmailConfig.Clone(),
                HtmlExtra = HtmlExtra.Clone(),
                ImgData = ImgData.Clone(),
                Batch = Batch.Clone(),
                isFileLog = isFileLog,
                IValidCache = IValidCache,
                isAutoScroll = isAutoScroll
            };
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }

    [Serializable]
    public class EmailConfig : IValid, Data.ITClonable<EmailConfig>
    {
        [NonSerialized]
        public Button TestSend;
        public class PortProperty
        {
            public int PortNumber { get; set; }
            public int Id { get; set; }
        }
        private string _server;
        private int _port;
        private string _sender;
        private string _senderPw;
        private bool _valid;
        public string TestEmail { get; set; }
        public string Server
        {
            get { return _server; }
            set
            {
                _server = value;
                ValidateNow();
            }
        }

        public int Port
        {
            get { return _port; }
            set
            {
                _port = value;
                ValidateNow();
            }
        }

        public string Sender
        {
            get { return _sender; }
            set
            {
                _sender = value;
                ValidateNow();

            }
        }

        public string SenderPw
        {
            get { return _senderPw; }
            set
            {
                _senderPw = value;
                ValidateNow();
            }
        }

        public bool Valid
        {
            get { return _valid; }
            set { _valid = value; }
        }

        public void ValidateNow()
        {
            _valid = StringValidator.IsEmail(Sender) && !String.IsNullOrEmpty(SenderPw) && StringValidator.IsURL(Server);
            if (TestSend != null)
            {
                TestSend.IsEnabled = _valid;
            }
        }
        public static ObservableCollection<PortProperty> ComboBoxBindingFactoryPorts()
        {
            return new ObservableCollection<PortProperty>()
            {
                new PortProperty() {PortNumber = 25, Id = 0},
                new PortProperty() {PortNumber = 465, Id = 1},
                new PortProperty() {PortNumber = 587, Id = 2}
            };
        }

	    public EmailConfig()
	    {
		    Port = 587;
	    }
        public EmailConfig Clone()
        {
            return new EmailConfig()
            {
                Port = Port,
                Sender = Sender,
                SenderPw = SenderPw,
                Server = Server,
                Valid = Valid,
                TestEmail = TestEmail,
                TestSend = TestSend
            };
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }

    [Serializable]
    public class HtmlExtra: Data.ITClonable<HtmlExtra>
    {
        public DataBundleable Header { get; set; }
        public DataBundleable Footer { get; set; }

        public HtmlExtra()
        {
            Header = new DataBundleable("ExtraHead", true);
            Footer = new DataBundleable("ExtraFoot", true);
        }

        public HtmlExtra Clone()
        {
            return new HtmlExtra()
            {
                Header = Header.Clone(),
                Footer = Footer.Clone()
            };
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }

    [Serializable]
    public class ImgData : IValid, Data.ITClonable<ImgData>
    {
        public class AlignmentProperty
        {
            public string AlignType { get; set; }
            public int Id { get; set; }
        }
        private bool _valid;
        private int _width;
        private string _link;
        public string Subject { get; set; }
        public string Align { get; set; }
        public string Link
        {
            get { return _link; }
            set
            {
                _link = value;
                Console.WriteLine("Link: " + _link);
                ValidateNow();
            }
        }

        public DataBundleable Image { get; set; }

        public int Width
        {
            get { return _width; }
            set
            {
                _width = value;
                ValidateNow();
            }
        }

        public bool Valid
        {
            get { return _valid; }
            set { _valid = value; }
        }

        public ImgData()
        {
            Image = new DataBundleable("ImageLocation", false);
            Width = 80;
            Align = "Center";
            Link = "http://";

        }

        public void ValidateNow()
        {
            _valid = StringValidator.IsFullURL(Link) && (Width >= 5 && Width <= 100);
        }

        public static ObservableCollection<AlignmentProperty> ComboBoxBindingFactory()
        {
            return new ObservableCollection<AlignmentProperty>()
      {
        new AlignmentProperty() {AlignType = "Left", Id = 0},
        new AlignmentProperty() {AlignType = "Center", Id = 1},
        new AlignmentProperty() {AlignType = "Right", Id = 2}
      };
        }

        public ImgData Clone()
        {
            return new ImgData()
            {
                Link = Link,
                Width = Width,
                Valid = Valid,
                Subject = Subject,
                Align = Align,
                Image = Image.Clone()
            };
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }

    [Serializable]
    public class Batch : IValid, Data.ITClonable<Batch>, INotifyPropertyChanged
    {
        public float DelayMinutes
        {
            get { return _delayMinutes; }
            set
            {
                _delayMinutes = value;
                var t = TimeSpan.FromMinutes(value);
                _delaySeconds = t.Days* 86400 + t.Hours*3600 + t.Minutes*60 + t.Seconds;
                ValidateNow();
                Console.WriteLine("delaymin: " + _delayMinutes);
				Console.WriteLine("delayminSec: " + _delaySeconds);
                OnPropertyChanged();
                OnPropertyChanged(nameof(DelaySeconds));
            }
        }

        public int DelaySeconds
        {
            get { return _delaySeconds; }
            set
            {
                _delaySeconds = value;
                var t = TimeSpan.FromSeconds(value);
                _delayMinutes = (float)Math.Round(t.Hours*60 + t.Minutes + (float)t.Seconds / 60, 2);
                ValidateNow();
                //Console.WriteLine("delaysec: " + _delaySeconds);
                //Console.WriteLine("delaysecMin: " + _delayMinutes);
                OnPropertyChanged();
                OnPropertyChanged(nameof(DelayMinutes));
            }
        }

        [NonSerialized] public int Trims;
        [NonSerialized] public int EmptyCut;
        [NonSerialized] public int Invalid;
        public bool Sequential { get; set; }
        public int AmountPerSend
        {
            get { return _amountPerSend; }
            set
            {
                _amountPerSend = value;
	            Console.WriteLine("amount per send: " + _amountPerSend);
				ValidateNow();
            }
        }

        public bool Valid { get; set; }
        private float _delayMinutes;
        private int _amountPerSend;
        private int _delaySeconds;
        public DataBundleable EmailList { get; set; }
        public Batch()
        {
            EmailList = new DataBundleable("ListLocation", false);
            AmountPerSend = 5;
            DelayMinutes = 5;
            Sequential = false;
        }

        public void ValidateNow()
        {
            Valid = DelayMinutes > 0 && AmountPerSend > 0;
        }

        public Batch Clone()
        {
            return new Batch()
            {
                AmountPerSend = AmountPerSend,
                DelayMinutes = DelayMinutes,
                EmailList = EmailList.Clone(),
                Sequential = Sequential
            };
        }

        object ICloneable.Clone()
        {
            return Clone();
        }

		[field:NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
    
    [Serializable]
    public class DataBundleable : IValid, Data.ITClonable<DataBundleable>
    {
        public string ReferencePath
        {
            get { return _referencePath; }
            set
            {
                _referencePath = value;
                ValidateNow();
            }
        }
        public bool Valid { get; set; }
        public object Data { get; set; }
        public int DataHash { get; set; }
        public Type DataType;
        public string TextboxName;
        public bool Optional;
        private string _referencePath;

        public DataBundleable(string TextboxReferenceName, bool optional)
        {
            Optional = optional;
            TextboxName = TextboxReferenceName;
        }
        //Use during build to build the Data file, then reference it by casting from
        public Tuple<string, bool> DataBuilder<T>(string reference)
        {
	        if (String.IsNullOrEmpty(ReferencePath) && Optional)
	        {
		        if (string.IsNullOrEmpty(ReferencePath)) Data = null;
		        return Tuple.Create("Unused optional", true);
	        }
            DataType = typeof(T);
            if (!File.Exists(reference) && Data != null)
            {
                Console.WriteLine("preserve old data, path not exist.");
                return Tuple.Create("Path doesn't exist, preserve old data", true);
            }
            ReferencePath = reference;
            if (DataHash != 0)
            {
                if (DataHash == Utilities.FolderAndFiles.FileHash(reference))
                {
                    Console.WriteLine("Data already process " + ReferencePath);
                    return Tuple.Create("Data already process " + ReferencePath, true);
                }
            }
            var returnMessage = Tuple.Create("" + ReferencePath, false);
            if (DataType == typeof(string))
            {
                returnMessage = ConvertDataString();
            }
            if (DataType == typeof(byte[]))
            {
                returnMessage = ConvertDataByte();
            }
            if (DataType == typeof(string[]))
            {
                returnMessage = ConvertDataStringArray();
            }
            if (returnMessage.Item2)
            {
                DataHash = Utilities.FolderAndFiles.FileHash(reference);
                return returnMessage;
            }
            return Tuple.Create("Unknown data type: " + ReferencePath, false); ;
        }

        private Tuple<string, bool> ConvertDataString()
        {
            try
            {
                Data = File.ReadAllText(ReferencePath);
                return Tuple.Create("Load data: " + ReferencePath, true);
            }
            catch
            {
                return Tuple.Create("Failed load data: " + ReferencePath, false);
            }
        }
        private Tuple<string, bool> ConvertDataByte()
        {
            try
            {
                Data = File.ReadAllBytes(ReferencePath);
                return Tuple.Create("Load data: " + ReferencePath, true);
            }
            catch
            {
                return Tuple.Create("Failed load data: " + ReferencePath, false);
            }
        }
        private Tuple<string, bool> ConvertDataStringArray()
        {      
            try
            {
                var rawList = File.ReadAllLines(ReferencePath);
                var rawListList = rawList.ToList();
                var rawListListT = new List<string>(rawListList);
                var emptyCut = 0;
                var trim = 0;
                rawListList.ForEach((e) =>
                {
                    if (e == "")
                    {
                        rawListListT.Remove(e);
                        emptyCut++;
                    }
                });
                for (int i = 0; i < rawListListT.Count; i++)
                {
                    var r = rawListListT[i].Trim();
                    if (r != rawListListT[i])
                    {
                        rawListListT[i] = r;
                        trim++;
                    }
                }
                Data = rawListListT.ToArray();
                var returnMsg = emptyCut + ":" + trim;
                return Tuple.Create(returnMsg, true);
            }
            catch
            {
                return Tuple.Create("Failed load data: " + ReferencePath, false);
            }
        }

        public string DatatoString()
        {
            return (string)Data;
        }
        public void ValidateNow()
        {
            if (String.IsNullOrEmpty(ReferencePath) && Optional)
            {
                Valid = true;
            }
            else
            {
                Valid = File.Exists(ReferencePath);
            }
            if (!Valid) Console.WriteLine("Databundleable is false! " + ReferencePath);
        }

        public DataBundleable Clone()
        {
            return new DataBundleable(TextboxName, Optional)
            {
                ReferencePath = ReferencePath,
                Data = Data,
                DataType = DataType,
                Valid = Valid,
                DataHash = DataHash
            };
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
