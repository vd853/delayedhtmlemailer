﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using EmailWordDelay.Annotations;
using Microsoft.Win32;
using Utilities;

namespace EmailWordDelay
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window, INotifyPropertyChanged
		//INotifyPropertyChanged minimum implimentation:
		//public event PropertyChangedEventHandler PropertyChanged;
    {
	    public event PropertyChangedEventHandler PropertyChanged;
        public static MainWindow M;
		public ObservableCollection<ImgData.AlignmentProperty> ComboAlignment { get; set; }
		public ObservableCollection<EmailConfig.PortProperty> ComboPort { get; set; }
		private string _Logbox;
		private FileLogL4N L;

		public TimedMain TimedMain { get; set; }

		public BundleData B
		{
			get { return _b; }
			set
			{
				_b = value;
			}
		}

		public string Logbox
		{
			get { return this._Logbox; }
			set {
                this._Logbox = value;
			}
		}
		private FileSerialization DataManager;
		private List<TextBox> StringBox = new List<TextBox>(); //password box not included
		private List<TextBox> NumberBox = new List<TextBox>();
		private PasswordBox EmailPassword;
		private Brush defaultBorderColor;
		private Brush defaultBorderColorButton;
		private List<DataBundleable> Bundleables;
		private List<IValid> BuildValidators;
		private bool initBuild;
		private List<string> tempFilePath = new List<string>(); //delete all on exit
		private static BundleData _b;
		private const string defaultDataFile = "Data.DE";
		private string defaultTmpImagePath = Directory.GetCurrentDirectory() + "\\tmpImg.jpg";
        private bool BundleableModified = true;
        private Task Builder, HTMLGenerator;
        private string secret = "unlocker";
        private IEnumerable<Control> InactiveWhileActive;

        public MainWindow()
		{
			InitializeComponent();
			M = this;
			Logbox = "Logs: " + Environment.NewLine;
			defaultBorderColor = ExtraHead.BorderBrush;
			defaultBorderColorButton = Save.BorderBrush;	
		    DataContext = this;
            InitData();
            RegisterTextBoxes();
		    RegisterTooltip();
            ButtonToggle();
			Align.SelectedIndex = 0;
			Port.SelectedIndex = 0;
			debug(Visibility.Hidden);
			//EnableBuild();
			RegisterBindings();
            //Timers.Visibility = Visibility.Hidden;
		    TestEmail.Text = B.EmailConfig.Sender;
		}

        
        private void Debug_Click(object sender, RoutedEventArgs e)
        {
            //ToggleConfigTabs(true);
        }
        void debug(Visibility visible)
		{
			Debug.Visibility = visible;
		}
		void InitData()
		{
            var file = "";
			var relative = true;
			try
			{
				DataManager = new FileSerialization(defaultDataFile, BundleData.Factory(), relative);
				B = DataManager.CipherLoad(secret) as BundleData;
			}
			catch
			{
				Logbox += defaultDataFile + " is corrupted. File will be rebuild." + Environment.NewLine;
				try
				{
					File.Delete(Directory.GetCurrentDirectory() + "/" + defaultDataFile);
				}
				catch
				{
					MessageBox.Show("Failed to delete " + defaultDataFile + " delete it manually and open this application again.");
					Environment.Exit(0);
				}
				DataManager = new FileSerialization(defaultDataFile, BundleData.Factory(), relative);
				B = DataManager.CipherLoad(secret) as BundleData;
			}
		    Console.WriteLine("Init file: " + DataManager.FileName);
            DataReload();
		}

		//private void LogOption_Click(object sender, RoutedEventArgs e)
		//{
		//	var c = sender as CheckBox;
		//	B.isFileLog = c.IsChecked.Value;
		//	DataManager.CipherSave(B, secret);
		//}

		//Use as importer
		private void Load_Click(object sender, RoutedEventArgs e)
		{
		    OnFileSelectDE(sender, e);
		}

        //Use as exporter
        private async void Save_Click(object sender, RoutedEventArgs e)
		{
			var diag = new SaveFileDialog();
			diag.Filter = StringFormatter.DialogFilterGenerator(new[] { "Delay Email", ".DE" });
			if (diag.ShowDialog() == true)
			{
				if (diag.FileName != Directory.GetCurrentDirectory() + "\\" + defaultDataFile)
				{
				    await BuildNow();
				    DataManager.CipherSave(B, secret);
                    FileSerialization.SaveOut(ref DataManager, diag.FileName, true);
				}
				else
				{
					MessageBox.Show("You cannot export as the data file name at this location: " + defaultDataFile);
				}
			}
		}

		private void Window_Closing(object sender, CancelEventArgs e)
		{
		    TimedMain.Stop();
            DataManager.CipherSave(B, secret);
		    var sw = new Stopwatch();
            sw.Start();
		    foreach (var s in tempFilePath)
		    {
		        while (File.Exists(s))
		        {
		            try
		            {
		                File.Delete(s);
		            }
		            catch
		            {
		            }
		            if (sw.ElapsedMilliseconds > 3000) //timeout if cannot delete
		            {
		                return;
		            }
		        }
		    }
        }

		private void Preview_Click(object sender, RoutedEventArgs e)
		{
		    if (B.ImgData.Image.Data != null)
		    {
		        HTMLBuilder.PreviewHTML(B);
		    }
		    else
		    {
		        Console.WriteLine("imageData is null");
		    }
		}

		private async void Start_Click(object sender, RoutedEventArgs e)
		{
		    var answer = Dialog.YesNo("Do you really want to start? ", new Action(() => { }));
		    if (!answer) return;
		    Logger("", false, true);
		    Logger("STARTING ALL TASK");
		    OnStartLogs();
		    ToggleStarterControls(OperateMode.Sending);
		    Timers.Visibility = Visibility.Visible;
		    Console.WriteLine("click started");
		    CreateJobs(false);
		    await TimedMain.Start(B.Batch.Sequential);
        }

		private void Pause_Click(object sender, RoutedEventArgs e)
		{
			TimedMain.Pause();
		}

		private void Stop_Click(object sender, RoutedEventArgs e)
		{
			TimedMain.Stop();
		}

		private async void Test_Click(object sender, RoutedEventArgs e)
		{
			var def = Test.Content;
			var defC = Test.Background;
            Test.Content = "Testing...";
            Test.Background = Brushes.LightCoral;
            var tmp = B.Clone();
		    var testEmail = TestEmail.Text;
            var re = await Task.Run(()=>
            {
                var s = new Sender();
                return s.Send(tmp, testEmail, new HTMLBuilder(defaultTmpImagePath, tmp));
            });
			if (!re)
			{
				MessageBox.Show("Error sending email. Check your settings.");
			}
			else
			{
				MessageBox.Show("Email sent. Check your inbox.");
			}
			Test.Content = def;
			Test.Background = defC;
		}

        

        private async void Rebuild_Click(object sender, RoutedEventArgs e)
        {
	        var success = await BuildNow();
            if (success)
            {
                CheckEmailList(true);
                Logger("Rebuild is successful.");
            }
            else
            {
                Logger("Rebuild is unsuccessful.");
            }
        }

		private void Port_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			Console.WriteLine(B.EmailConfig.Port);
			OnDataChange();
		}
		private void Align_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			Console.WriteLine(B.ImgData.Align);
			OnDataChange();
		}

		private void About_Click(object sender, RoutedEventArgs e)
		{
		    MessageBox.Show(
		        "This application is use to send email batches with delayed timer. Extra features includes HTML append of a header and footer. Fields that are highlighted in green are data that can be bundled and do not need validation after import.", "About");
		}
		private async void Simulate_Click(object sender, RoutedEventArgs e)
        {
	        ToggleStarterControls(OperateMode.Sending);
	        Timers.Visibility = Visibility.Visible;
	        Console.WriteLine("click started");
	        CreateJobs(true);
            Logger("", false, true);
			Logger("SIMULATION BEGINS");
            OnStartLogs();
            await TimedMain.Start(B.Batch.Sequential);
		}
    }
}
