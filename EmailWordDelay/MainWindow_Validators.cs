﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using Utilities;

namespace EmailWordDelay
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow
	{
		void NumbersOnlyMaximzed(object s, TextChangedEventArgs e)
		{
			var tb = s as TextBox;
			if (!StringValidator.OnlyNumber(tb.Text, 4, true))
			{
				tb.Text = "9999";
				tb.SelectAll();
			}
		}
	    void NumbersOnlyMaximzedDecimal(object s, TextChangedEventArgs e)
	    {
	        var tb = s as TextBox;
	        if (tb.Text == ".") return;
	        if (!StringValidator.isDecimal(tb.Text, 0, 129600))
	        {
	            tb.Text = "0";
	            tb.SelectAll();
	        }
	    }
        void ValidateBuild()
		{
			Console.WriteLine("validating build");
			//will not be buildable if non-optional reference path for data is not defined
		    if (BundleableModified)
		    {
		        BundleableModified = false;
                foreach (var dataBundleable in Bundleables)
		        {
		            if (!dataBundleable.Valid)
		            {
		                DisableBuild();
		                return;
		            }
		        } 
		    }
			foreach (var v in BuildValidators)
			{
				if (!v.Valid)
				{
					DisableBuild();
					return;
				}
			}
		    Console.WriteLine("is valid");
		    EnableBuild();
		}
		void OnUseIndicate()
		{
			Bundleables.ForEach((i) =>
			{
				var TB = this.FindName(i.TextboxName) as TextBox;
				TB.Background = Brushes.DarkSeaGreen;
			});
		    if (Utilities.TaskProcess.TaskInProgress(ref HTMLGenerator)) return;
            HTMLGenerator = Task.Run(()=> PregenerateImage());
		}

		void OnUseIndicatorDiable()
		{
			Bundleables.ForEach((i) =>
			{
				var TB = this.FindName(i.TextboxName) as TextBox;
				TB.Background = Brushes.White;
			});
		}
		void EnableBuild()
		{
            //Save.Background = Brushes.LightCoral;
            //Bundle.Background = Brushes.LightCoral;
		    Save.IsEnabled = true;
            if (Utilities.TaskProcess.TaskInProgress(ref Builder)) return;
		    Builder = Task.Run(() => BuildNow());
		}
		void DisableBuild()
		{
			ToggleStarterControls(OperateMode.NotReady);
			Save.Background = defaultBorderColorButton;
			Bundle.Background = defaultBorderColorButton;
			OnUseIndicatorDiable();
		}
		void ButtonToggle(bool save = true, bool load = true)
		{
			Save.IsEnabled = save;
			Load.IsEnabled = load;
		}

		enum OperateMode
		{
			Sending,
			NotReady = 1,
			Ready,
			Paused
		}
		void ToggleStarterControls(OperateMode Mode)
		{
			if (Mode == OperateMode.NotReady)
			{
				State.Content = "Invalid configs";
			}
			else
			{
				State.Content = Mode.ToString();
			}
			switch (Mode)
			{
				case OperateMode.Sending:
					Timers.Background = Brushes.LightCoral;
					Start.IsEnabled = false;
					Stop.IsEnabled = true;
					Pause.IsEnabled = true;
					Rebuild.IsEnabled = false;
					Simulate.IsEnabled = false;
                    Bundle.IsEnabled = false;
				    ToggleConfigTabs(false, new List<string>());
                    break;
				case OperateMode.NotReady:
					Timers.Background = Brushes.LightGray;
				    ToggleConfigTabs(true, new List<string>() {"Test"});
                    Start.IsEnabled = false;
					Stop.IsEnabled = false;
					Pause.IsEnabled = false;
					TimedMain.SetTimerInactive();
					Rebuild.IsEnabled = false;
				    Preview.IsEnabled = false;
					Simulate.IsEnabled = false;
				    Save.IsEnabled = false;
                    break;
				case OperateMode.Paused:
					Timers.Background = Brushes.PeachPuff;
					Start.IsEnabled = false;
					Stop.IsEnabled = true;
					Pause.IsEnabled = true;
					Rebuild.IsEnabled = false;
					Simulate.IsEnabled = false;
				    Bundle.IsEnabled = false;
				    ToggleConfigTabs(false, new List<string>());
                    break;
				case OperateMode.Ready:
					Timers.Background = Brushes.LightGreen;
					Start.IsEnabled = true;
					Stop.IsEnabled = false;
					Pause.IsEnabled = false;
					Rebuild.IsEnabled = true;
					Simulate.IsEnabled = true;
                    TimedMain.SetTimerInactive();
				    Save.IsEnabled = true;
				    Bundle.IsEnabled = true;
				    ToggleConfigTabs(true, new List<string>());
                    break;
				default:
					throw new ArgumentOutOfRangeException(nameof(Mode), Mode, null);
			}
		}

	}
}
