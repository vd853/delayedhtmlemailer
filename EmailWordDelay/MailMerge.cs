﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Spire.Doc;
using Spire.Doc.Documents;

namespace EmailWordDelay
{
    public class MailMerge
    {
        public static void Send(string path)
        {

            Document doc = new Document();
            doc.LoadFromFile(path);
            //Mail Merge
            string[] filedNames = new string[] { "receiver_Name", "receiver_address", "Sender_Name", "Sender_Address", "Date"};
            string[] filedValues = new string[] { "Branka", "viluan@dvicomm.com", "Lartias", "viluan@dvicomm.com", System.DateTime.Now.Date.ToString() };
            doc.MailMerge.Execute(filedNames, filedValues);

            //Save and Launch
            //document.SaveToFile(“MailMerge.docx”, FileFormat.Docx);
            //System.Diagnostics.Process.Start(“MailMerge.docx”);

        }

    }
}
