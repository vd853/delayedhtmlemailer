﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace EmailWordDelay
{
    public class Element
    {
        //returns all children and subchildren from parent rootObject
        private static List<DependencyObject> RecursiveSearch(DependencyObject rootObject)
        {
            var list = new List<DependencyObject>();
            Console.WriteLine("finding next child: " + VisualTreeHelper.GetChildrenCount(rootObject));
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(rootObject); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(rootObject, i);
                list.Add(child);
                list = list.Concat(RecursiveSearch(child)).ToList();
            }
            return list;
        }

        /// <summary>
        /// Returns all children and subchildren base on the filter list as a list of Control. Only works on GRID and STACKPANEL! Doesn't work with tabs...
        /// </summary>
        /// <param name="rootObject">Parent element to search within</param>
        /// <param name="includeTypes">List of types to include ex. new List<Type>() {typeof(TextBox)... </param>
        /// <param name="excludeNames">List of x: Names to exclude from result ex. new List<string>(){"Main"... </param>
        /// <returns></returns>
        public static List<Control> FilteredSubChildren(DependencyObject rootObject, List<Type> includeTypes, List<string> excludeNames)
        {
            var list = new List<Control>();
            var unFilter = RecursiveSearch(rootObject);
            foreach (var dependencyObject in unFilter)
            {
                if (dependencyObject is Control)
                {
                    var c = dependencyObject as Control;
                    foreach (var includeType in includeTypes)
                    {
                        if (dependencyObject.GetType().Name == includeType.Name)
                        {
                            if (!excludeNames.Contains(c.Name))
                            {
                                Console.WriteLine("Qualified: " + c.Name);
                                list.Add(c);
                            } 
                        }
                    }
                }  
            }
            return list;
        }

    }
}


