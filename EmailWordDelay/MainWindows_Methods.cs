﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Utilities;

namespace EmailWordDelay
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public void AsyncLogger(string message, bool isError = false, bool isEmpty = false)
        {
            Dispatcher.Invoke(() => Logger(message, isError, isEmpty));
        }
        public void Logger(string message, bool isError = false, bool isEmpty = false)
        {
            if (isEmpty)
            {
                Logbox += Environment.NewLine;
                return;
            }
            Logbox += DateTime.Now.ToString("hh:mm:ss tt") + ": " + message + Environment.NewLine;
            if (B.isFileLog)
            {
                if (L == null)
                {
                    L = new FileLogL4N(FileLogL4N.GenerateFileName(isCSV:true));
                }
                if (isError)
                {
                    L.L.Error(message);
                    return;
                }
                L.L.Info(message);
            }
            if (B.isAutoScroll)
            {
                LogScroller.ScrollToBottom();
            }
        }

        private void PregenerateImage()
        {
            var imageData = (byte[])B.ImgData.Image.Data;
            if (imageData != null)
            {
                Console.WriteLine("Img bytes[] " + imageData.Length);
                foreach (var s in HTMLBuilder.PreviewHTML(B, true))
                {
                    tempFilePath.Add(s);
                }
            }
        }

        private async Task<bool> BuildNow() //on textentry and adding this will cause symptom of "double binding", but not really
        {
            Console.WriteLine("Building now");
            var tempData = B.Clone();
            var success = await Task.Run(() => BundleData.BuildBundle(tempData));
            if (!success.Item2) return success.Item2;
            Console.WriteLine("build success: " + success.Item1);
            B.Batch.EmailList.Data = success.Item3.Batch.EmailList.Data;
            B.ImgData.Image.Data = success.Item3.ImgData.Image.Data;
            B.HtmlExtra.Footer.Data = success.Item3.HtmlExtra.Footer.Data;
            B.HtmlExtra.Header.Data = success.Item3.HtmlExtra.Header.Data;
            Dispatcher.Invoke(() =>
            {
                DataReload();
                ButtonToggle();
                ReadyPreviewAndSave();
                ToggleStarterControls(OperateMode.Ready);
                OnUseIndicate();
            });
            if (!initBuild)
            {
                initBuild = true;
                CheckEmailList(true);
            }
            else
            {
                CheckEmailList();
            }
            return success.Item2;
        }

        void ReadyPreviewAndSave()
        {
            Preview.IsEnabled = true;
            Save.IsEnabled = true;
        }

        #region CreateJobs and Callback Actions
        void CreateJobs(bool isTest)
        {
            var l = new List<TimedUnit>();
            var emailList = (string[])B.Batch.EmailList.Data;
            foreach (var s in emailList)
            {
                var recipient = s;
                var reference = new WorkData(recipient, TimedMain);
                var _isTest = isTest;
                Func<WorkData, WorkData> work = input =>
                {
                    if (TimedMain.TokenSource.IsCancellationRequested)
                    {
                        input.Success = false;
                        return input;
                    }
                    var HTMLBuilder = new HTMLBuilder(defaultTmpImagePath, B);
                    var sender = new Sender();
                    var result = sender.Send(B, recipient, HTMLBuilder, _isTest);
                    if (result)
                    {
                        input.Success = true;
                    }
                    else
                    {
                        input.Success = false;
                    }
                    while (input.TimedMain._isPaused)
                    {
                        if (TimedMain.TokenSource.IsCancellationRequested)
                        {
                            input.Success = false;
                            return input;
                        }
                        Console.WriteLine("currently paused: " + input.Description);
                        Task.Delay(500).Wait();
                    }
                    return input;
                };
                var unit = TimedUnit.TestUnitFactory(work, reference);
                l.Add(unit);
            }
            TimedMain.InitNewTask(l,
                CompletedCallback(),
                CancelledCallback(),
                PausedCallback()
            );
        }
        Action CompletedCallback()
        {
            return () =>
            {
                AsyncLogger("All tasks completed.");
                Dispatcher.Invoke(() => ToggleStarterControls(OperateMode.Ready));
            };
        }

        Action CancelledCallback()
        {
            return () =>
            {
                AsyncLogger("All pending task was cancelled.");
                Dispatcher.Invoke(() => ToggleStarterControls(OperateMode.Ready));
            };
        }

        Action<bool> PausedCallback()
        {
            return (pauseState) =>
            {
                var msg = "Remain tasks are paused.";
                if (!pauseState)
                {
                    msg = "Remain tasks are unpaused.";
                    Dispatcher.Invoke(() => ToggleStarterControls(OperateMode.Sending));
                }
                else
                {
                    Dispatcher.Invoke(() => ToggleStarterControls(OperateMode.Paused));
                }
                AsyncLogger(msg);
            };
        }


        #endregion

        //None adjusting methods
        //      public event PropertyChangedEventHandler PropertyChanged;

        //[NotifyPropertyChangedInvocator]
        //protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        //{
        //	PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        //}

        void MockJobs()
        {
            var l = new List<TimedUnit>();
            for (int i = 0; i < 30; i++)
            {
                var des = "Test" + i.ToString();
                var referenceData = new WorkData(des, TimedMain);
                Func<WorkData, WorkData> TestWork = input =>
                {
                    var ran = Data.RandomInt(0, 2);
                    var delay = Data.RandomInt(1, 5) * 50;
                    var message = "runing " + input.Description + " with delay " + delay;
                    Task.Delay(delay).Wait();
                    while (input.TimedMain._isPaused)
                    {
                        if (TimedMain.TokenSource.IsCancellationRequested)
                        {
                            referenceData.Success = false;
                            return referenceData;
                        }
                        Console.WriteLine("currently paused1: " + input.Description);
                        Task.Delay(500).Wait();
                    }
                    if (TimedMain.TokenSource.IsCancellationRequested)
                    {
                        referenceData.Success = false;
                        return referenceData;
                    }
                    if (ran == 1)
                    {
                        Console.WriteLine(message);
                        referenceData.Success = true;
                        return referenceData;
                    }
                    Console.WriteLine(message);
                    referenceData.Success = false;
                    return referenceData;
                };
                Console.WriteLine("created: " + des);
                var u = TimedUnit.TestUnitFactory(TestWork, referenceData);
                l.Add(u);
            }
            TimedMain.InitNewTask(l,
                CompletedCallback(),
                CancelledCallback(),
                PausedCallback()
            );
        }

        private int? currentListCount;
        private void CheckEmailList(bool alwaysLog = false) //only works after successful rebuild, only logs if list count changes, List items are trimmed at DataBundleable.Databuilder.ConvertDataStringArray()
        {
            if (alwaysLog) currentListCount = -1;
            var recipientCount = (string[])B.Batch.EmailList.Data;
            var invalidCount = 0;
            if (currentListCount != null)
            {
                if (recipientCount.Length != currentListCount)
                {
                    currentListCount = recipientCount.Length;
                    var invalids = ValidateEmailList(recipientCount);
                    if (invalids.Any())
                    {
                        MessageBox.Show("There are " + invalids.Count + " possible invalid emails. Fix your list and rebuild again to check. Otherwise, email sent will still be attempted.");
                        invalidCount = invalids.Count;
                    }
                }
                else
                {
                    return;
                }
            }
            else
            {
                currentListCount = recipientCount.Length;
            }
            AsyncLogger("Recipient total: " + recipientCount.Length);
            if (invalidCount > 0)
            {
                AsyncLogger("Recipient invalid: " + invalidCount);
            }
            if (B.Batch.EmptyCut > 0)
            {
                AsyncLogger("Recipient empty: " + B.Batch.EmptyCut);
            }
            if (B.Batch.Trims > 0)
            {
                AsyncLogger("Recipient trimmed: " + B.Batch.Trims);
            }
        }

        private List<string> ValidateEmailList(string[] list)
        {
            var Invalids = new List<string>();
            foreach (var e in list)
            {
                Console.WriteLine("email check " + e);
                if (!Utilities.StringValidator.IsEmail(e))
                {
                    Invalids.Add(e);
                    AsyncLogger(e + " is invalid?");
                }
            }
            return Invalids;
        }
        void DataReload()
        {
            B.EmailConfig.TestSend = Test;
            Bundleables = new List<DataBundleable>();
            Bundleables.Add(B.ImgData.Image);
            Bundleables.Add(B.Batch.EmailList);
            Bundleables.Add(B.HtmlExtra.Footer);
            Bundleables.Add(B.HtmlExtra.Header);
            FindIValids();
            if (!String.IsNullOrEmpty(B.EmailConfig.SenderPw))
            {
                EmailSenderPassword.Password = B.EmailConfig.SenderPw;
            }
            TimedMain = new TimedMain(B);
        }
        void FindIValids()
        {
            var ValidateNows = B.GetSelfIValids();
            ValidateNows.ForEach((e) => e.ValidateNow());
            BuildValidators = ValidateNows.Except(Bundleables).ToList();
        }

        void OnStartLogs()
        {
            CheckEmailList(true);
            Logger("Delay: " + B.Batch.DelayMinutes + " minutes");
            Logger("Batch amount: " + B.Batch.AmountPerSend);
            Logger("Sequential: " + B.Batch.Sequential);
        }
        void ToggleConfigTabs(bool isActive, List<string> exluder)
        {
            //init
            if (InactiveWhileActive == null)
            {
                var exclude = exluder;
                var include = new List<Type>() { typeof(TextBox), typeof(CheckBox), typeof(ComboBox), typeof(PasswordBox), typeof(Button) };
                InactiveWhileActive = Element.FilteredSubChildren(BatchFinder, include, exclude).Concat(
                    Element.FilteredSubChildren(ImageFinder, include, exclude).Concat(
                        Element.FilteredSubChildren(EmailFinder, include, exclude).Concat(
                            Element.FilteredSubChildren(HTMLFinder, include, exclude)
                        )));
            }
            var e = InactiveWhileActive.GetEnumerator();
            while (e.MoveNext())
            {
                e.Current.IsEnabled = isActive;
            }

        }
    }
}
