﻿using System.Net.Mail;
using System.Threading.Tasks;

namespace EmailWordDelay
{
	public class Sender
	{
		public bool Send(
            BundleData B, 
            string recipientAddress, 
            HTMLBuilder HTMLBuilder,
			bool isTest = false)
		{
			try
			{
				if (!isTest)
				{
				    string server = B.EmailConfig.Server;
				    SmtpClient client = new SmtpClient(server, B.EmailConfig.Port);
				    string fromAddress = B.EmailConfig.Sender;
				    string password = B.EmailConfig.SenderPw;
				    MailAddress from = new MailAddress(fromAddress);
				    MailMessage message = new MailMessage();
				    message.From = from;
				    message.To.Add(new MailAddress(recipientAddress));

				    message.Subject = B.ImgData.Subject;
				    message = HTMLBuilder.AppendPrebuilt(message);
				    client.EnableSsl = true;
				    client.Credentials = new System.Net.NetworkCredential(fromAddress, password);
                    client.Send(message);
				    message.Dispose();
                }
				else
				{
					Task.Delay(Utilities.Data.RandomInt(50, 150)).Wait();
				}				
				return true;
			}
			catch
			{
				return false;
			}
		}
	}
}
