﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace EmailWordDelay
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        void RegisterBindings()
        {
            ComboAlignment = ImgData.ComboBoxBindingFactory();
            ComboPort = EmailConfig.ComboBoxBindingFactoryPorts();
            SetLabelBinding(Now, "TimedMain.CurrentTime");
            SetLabelBinding(LastSent, "TimedMain.LastProcess");
            SetLabelBinding(Next, "TimedMain.NextProcessTime");
            SetLabelBinding(Remain, "TimedMain.AmountRemaining");
        }

        void RegisterTooltip()
        {
	        var r = "This field is required.";
	        ListDelay.ToolTip = "Must be 1 or greater. Value can be in decimals.";
	        ListAmount.ToolTip = "Must be 1 or greater";
	        LogOption.ToolTip = "Creates or appends CSV log at application's path.";
	        Bundle.ToolTip = "Import or export a bundle which preserves all settings and data.";
	        Width.ToolTip = "Must be between 10% and 100%.";
	        ExtraHead.ToolTip = "(OPTIONAL) Adds HTML elements to email. DO NOT reference local sources. Right-clicking will clear this field.";
	        ExtraFoot.ToolTip = "(OPTIONAL) Adds HTML elements to email. DO NOT reference local sources. Right-clicking will clear this field.";
            ListLocation.ToolTip = "List of recipients in .txt. One recipient per line.";
	        ImageLink.ToolTip = "Must be of form http://...";
	        ImageLocation.ToolTip = r;
	        EmailSender.ToolTip = r;
	        EmailSenderPassword.ToolTip = r + " Tab out to confirm.";
	        EmailServer.ToolTip = r;
	        Simulate.ToolTip = "Start task without sending any email.";
	        Start.ToolTip = "Begin all task.";
	        Rebuild.ToolTip = "Reload all configurations. Usually not necessary";
	        Stop.ToolTip = "Cancel all running task including simulation.";
	        Pause.ToolTip = "Pause all running task including simulation.";
	        Preview.ToolTip = "Previews the email on your default web browser.";

            var timerDisplay = "Calendar Date:Hour:Minute:Second";
            Timers.ToolTip = timerDisplay;
            ScrollOption.ToolTip = "Scroll manually to stop auto-scroll when this is disabled."; 
        }
        void RegisterTextBoxes()
        {
            StringBox.Add(EmailServer);
            StringBox.Add(EmailSender);
            StringBox.Add(ImageSubject);
            StringBox.Add(ImageLink);

            ExtraHead.PreviewMouseLeftButtonDown += OnFileSelectHTML;
            ExtraFoot.PreviewMouseLeftButtonDown += OnFileSelectHTML;

	        ExtraHead.PreviewMouseRightButtonDown += OnRightClickClear;
			ExtraFoot.PreviewMouseRightButtonDown += OnRightClickClear;

			ImageLocation.PreviewMouseDown += OnFileSelectJPEG;

            ListLocation.PreviewMouseDown += OnFileSelectText;

            ListLocation.IsReadOnly = true;
            ExtraHead.IsReadOnly = true;
            ExtraFoot.IsReadOnly = true;
            ImageLocation.IsReadOnly = true;

            NumberBox.Add(Width);
            NumberBox.Add(ListDelay);
            NumberBox.Add(ListAmount);

            EmailSenderPassword.LostFocus += OnPWLostFocus;
            EmailSenderPassword.Password = B.EmailConfig.SenderPw;
            //LogOption.IsChecked = B.isFileLog;

            //URL varify
            EmailServer.TextChanged += OnURLEntry;
            ImageLink.TextChanged += OnURLEntry;
            ImageLink.LostFocus += OnURLExit;

            //Path varify
            //ListLocation.TextChanged += OnLocalPathEntry;
            //ImageLocation.TextChanged += OnLocalPathEntry;
            //ExtraHead.TextChanged += OnLocalPathEntry;
            //ExtraFoot.TextChanged += OnLocalPathEntry;

            StringBox.ForEach((b) =>
            {
                //b.TextChanged += OnTextEntry;
                b.LostFocus += OnFocusChangedSave;
                b.GotFocus += OnFocusSelectall;
            });

            NumberBox.ForEach((b) =>
            {
                //b.TextChanged += OnTextEntry;
                b.LostFocus += OnFocusChangedSave;
                b.GotFocus += OnFocusSelectall;
                b.TextChanged += NumbersOnlyMaximzed;
            });

            ListDelay.TextChanged -= NumbersOnlyMaximzed;
            ListDelay.TextChanged += NumbersOnlyMaximzedDecimal;
            TestEmail.GotFocus += OnFocusSelectall;
        }

        void SetLabelBinding(Label label, string path)
        {
            var b = new Binding(path);
            label.SetBinding(Label.ContentProperty, b);
        }
    }
}
