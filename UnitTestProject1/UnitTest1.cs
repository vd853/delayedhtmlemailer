﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security;
using System.Security.Cryptography;
using System.Threading;
using EmailWordDelay;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utilities;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestSend()
        {
            var m = new MailMessage();
            var imgsrc = @"C:\Users\viluan\Source\Repos\EmailWordDelay\EmailWordDelay\bin\Debug\DVi.jpg";
            //var h = EmailWordDelay.HTMLBuilder.altViewBuilder(ref m, imgsrc, "www.dvicomm.com", 80, "left");

        }

        [TestMethod]
        public void timeDisplay()
        {
            var next = DateTime.Now.AddMinutes(1);
            var s = new Stopwatch();
            s.Start();
            Thread.Sleep(4000);
            var remaining = new TimeSpan(0, 1, 0) - s.Elapsed;
            Trace.WriteLine("Next: " + next.ToString("hh\\:mm\\:ss"));
            Trace.WriteLine("Now: " + DateTime.Now.ToString("hh\\:mm\\:ss"));
            Trace.WriteLine("Time Countdown: " + remaining.ToString("hh\\:mm\\:ss"));
        }

        [TestMethod]
        public void URLValidate()
        {
            Trace.WriteLine(StringValidator.IsURL("w.google??.com"));
        }

        [TestMethod]
        public void getExtensions()
        {
            var l = Utilities.FileSerialization.DiscoverFile(".dll");
            foreach (var s in l)
            {
                Trace.WriteLine(s);
            }
        }
        [TestMethod]
        public void ExtenstionFilter()
        {
            var s = new[] { "Delay Email", ".DE" };
            Assert.AreEqual("Delay Email (*.DE)|*.DE|All files (*.*)|*.*", Utilities.StringFormatter.DialogFilterGenerator(s));
        }

        [TestMethod]
        public void GetAllVariblesFromClassOfInterfaceThenNameThenCacheThenGetBack()
        {
            var c = BundleData.Factory();
            var VList = c.GetSelfIValids();
        }

        [TestMethod]
        public void ReFile()
        {
            var modifyMe = new Utilities.FileSerialization("test.data", EmailWordDelay.BundleData.Factory());
            var data1 = (EmailWordDelay.BundleData) modifyMe.load();
            data1.EmailConfig.Sender = "FIRSTONE";
			modifyMe.save(data1);

            var loadFrom = new Utilities.FileSerialization("test2.data", EmailWordDelay.BundleData.Factory());
            var data2 = (EmailWordDelay.BundleData)loadFrom.load();
            data2.EmailConfig.Sender = "NEWONE";
	        loadFrom.save(data2);

			Utilities.FileSerialization.ReFile<EmailWordDelay.BundleData>(ref modifyMe,ref data1, loadFrom.FileName, false, false);

            Assert.AreEqual(data2.EmailConfig.Sender, data1.EmailConfig.Sender);
        }

	    [TestMethod]
	    public void predicateInvokeTest()
	    {
		    Predicate<int> test = (int input) =>
		    {
			    Trace.WriteLine("OKAYcompleted");
			    return true;
		    };
		    Trace.WriteLine("OKAY" + test.Invoke(0));
			;
	    }

        [TestMethod]
        public void EncryptionString()
        {
            Trace.WriteLine("nothing");
            var e = Utilities.Cipher.EncryptionPlainText("ab", "hello");
            var r = Utilities.Cipher.DecryptionPlainText(e, "hello");
            Trace.WriteLine(e);
            Assert.AreEqual("ab", r);
        }

        [TestMethod]
        public void StringToObject()
        {
            var t = new FileSerialization("test.data", EmailWordDelay.BundleData.Factory());
            var obj = t.load();
            var actual = (BundleData)obj;
            FileStream file = System.IO.File.Open("test.data", FileMode.Open);
            //memorystream for byte[]

            var str = Utilities.Data.ObjectToString(obj);
            var objReturn = Utilities.Data.StringToObject(str);

            var expect = Utilities.Data.ObjectToByteArray(obj);
            var actually = Utilities.Data.ObjectToByteArray(objReturn);

            CollectionAssert.AreEqual(expect.ToList(), actually.ToList());
        }
        [TestMethod]
        public void ObjectCipher()
        {
            var t = new FileSerialization("test.data", EmailWordDelay.BundleData.Factory());
            var obj = t.load();
            var actual = (BundleData)obj;
            FileStream file = System.IO.File.Open("test.data", FileMode.Open);

            var encrypt = Utilities.Cipher.EncryptionObject(obj, "hello");
            var decrypt = Utilities.Cipher.DecryptionObject(encrypt, "hello");

            Assert.IsTrue(Utilities.Data.ObjectByBComparison(obj, decrypt));
        }

        [TestMethod]
        public void CipherDataFile()
        {
            var t = new Utilities.FileSerialization("testCipher.data", EmailWordDelay.BundleData.Factory());
            var obj = t.CipherLoad("unlocker");
            var objClass = (EmailWordDelay.BundleData) obj;
            Assert.AreEqual(5, objClass.Batch.DelayMinutes);
        }

        [TestMethod]
        public void TypeIClonable()
        {
            var t = new EmailWordDelay.test();
            t.value = "olllo";
            Assert.AreEqual(t.value, t.Clone().value);
        }

        [TestMethod]
        public void DataBundleClone()
        {
            var original = EmailWordDelay.BundleData.Factory();
            original.EmailConfig.Sender = "hello@nothing.com";
            original.ImgData.Subject = "wonderbread";
            original.HtmlExtra.Header.TextboxName = "foo";
            original.HtmlExtra.Footer.Data = new[] {"hello", "there"};
            var cOriginal = original.Clone();
            Assert.IsTrue(Utilities.Data.ObjectByBComparison(original,cOriginal));
        }

        [TestMethod]
        public void DecimalHours()
        {
            Trace.WriteLine(TimeSpan.FromMinutes(99999).ToString(Utilities.StringFormatter.TimeSpanDDDHHDMMDSSFormat()));
        }

        [TestMethod]
        public void DecimalOnly()
        {
            var test = false;
            try
            {
                decimal.Parse("-.010");
                test = true;
            }
            catch
            {
            }

            Assert.IsTrue(test);
        }

        [TestMethod]
        public void deltaFileTest()
        {
            Trace.WriteLine(Utilities.FolderAndFiles.FileHash(@"C:\Users\viluan\Source\Repos\EmailWordDelay\EmailWordDelay\bin\Debug\Batch_Test.txt"));
        }

        [TestMethod]
        public void AESEncryption()
        {
            //O3ABwhR9YwZPCMc3yuD40A==
            var en = Utilities.CipherCommon.Encrypt("value1", "pw");
            Trace.WriteLine(en);
            Assert.AreEqual("value1", Utilities.CipherCommon.Decrypt(en, "pw"));
        }

        [TestMethod]
        public void SecureStringTest()
        {
            //var s = new SecureString("something", MD5);
        }

        [TestMethod]
        public void EmailValidate()
        {
            var email = "fesfe@s.scm";
            var test = Utilities.StringValidator.IsEmail(email);
            Assert.IsTrue(test);
        }

        [TestMethod]
        public void getHash()
        {
        }
    }
}
