# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This application is use to send email batches with delayed time. The intent was to create a mass email delievery of Christmas cards, therefore it has an option to add and align a main image (the card) with a link (to the business). Extra features includes HTML append of a header and footer.

Features:

-Full timer status and logs

-Simulation without actually sending out the emails

-Full configuration validation in the UI

-Automatically cleans email list and validation in the UI

-Send batch sequentially or in parallel

-Adjustable image settings

-Email test and quick preview

-Append HTML header and footer (optional)

-File log

-Supports pausing and stopping

-Bundles configuration and data. Bundle can be imported or exported.

![Scheme](/EmailWordDelay/SS/0.jpg)
![Scheme](/EmailWordDelay/SS/1.jpg)
![Scheme](/EmailWordDelay/SS/2.jpg)
![Scheme](/EmailWordDelay/SS/3.jpg)
![Scheme](/EmailWordDelay/SS/4.jpg)

### How do I get set up? ###

Compiled version is just the exe and config file. https://drive.google.com/open?id=1jBX7pjK3dnUEI6F4iNW9i-ZKHA2CkkZG

### Contribution guidelines ###

### Who do I talk to? ###

* Repo owner or admin